---
bookCollapseSection: true
weight: 20
---

# Dokku Basics

## Installation
Installation via bootstrap script.
[Latest Dokku](https://github.com/dokku/dokku/releases)
```bash
wget https://raw.githubusercontent.com/dokku/dokku/v0.19.13/bootstrap.sh
sudo DOKKU_TAG=v0.19.13 bash bootstrap.sh
```

## Create New Project
My preference was, app naming should be separated by `-` example `admin-test`.  
To prevent confusion between database
```bash
$ dokku apps:create PROJECT_NAME
```

## Assign Domain
```bash
$ dokku domains:set PROJECT_NAME DOMAIN
```

## Mounting Persistent Volume
Persistent volume help us to make the data not temporary and still there after the container restarted.
Using docker as the basic, dokku provides out of the box support for mounting persistent volume. Dokku advised
to use the `/var/lib/dokku/data/storage/PROJECT_NAME/` as the folder to be mounted volume. After you create folder
in aboves directory, run below script to mount the persistent volume.  
`$ dokku storage:mount node-test /var/lib/dokku/data/storage/node-test/public:/public`

{{< hint info >}}
**Notes**  
If you run `$ dokku storage:report instance` it will show the folder on `deploy` and `run` section.
This means the mounting will be performed only on deploy and run. Any folder or file access that need to 
change the mounted volume should be done on this part
{{< /hint >}}

