---
title: Dokku Plugins
# draft: true
weight: 1
---

# Dokku Plugins 

## MariaDB
Because MySQL usually get buggy, its better we install MariaDB. 

### Installation [Latest](https://github.com/dokku/dokku-mariadb/releases)
This automatically pull latest version.

```bash
sudo dokku plugin:install https://github.com/dokku/dokku-mariadb.git mariadb  
```

{{< hint warning >}}
**Bug**  
As issues [#86: NO CONNECTION](https://github.com/dokku/dokku-mariadb/issues/86) still occurs, and we cannot specify which mariadb version we needed. Here's temporary workaround, revert to version 1.5.0

```bash
cd /var/lib/dokku/plugins/available/mariadb

# Switch to the 1.5.0 version
sudo git checkout 1.5.0 
```
{{< /hint >}}

### Create new database service

I prefer to name the service using `snake_case` to prevent confusion between naming the apps.
```bash
$ dokku mariadb:create test_db
```

### Linking service with apps
Dokku linking method was adding `DATABASE_URL` to the `dokku:config`.

```bash
# Remember to write the result!
$ dokku mariadb:link test_db api-test
```

### See Database Connection Info
```bash
$ dokku mariadb:info test_db

# Result
# mysql://mariadb:pwslakj1ixjoij@dokku-mariadb-test-db:3306/test_db
# DB_HOST=dokku-mariadb-test-db
# DB_NAME=test_db
# DB_USERNAME=mariadb
# DB_PASSWORD=pwslakj1ixjoij
```




## Let's Encrypt

### Installation - [Latest](https://github.com/dokku/dokku-mariadb/releases)
```bash
# dokku 0.5+
$ sudo dokku plugin:install https://github.com/dokku/dokku-letsencrypt.git
```

### Set Global / Single App Email
```bash
# global
$ dokku config:set --global DOKKU_LETSENCRYPT_EMAIL=pkotemon@gmail.com

# single app
$ dokku config:set api-test DOKKU_LETSENCRYPT_EMAIL=pkotemon@gmail.com
```

### Install Certificate
```bash
$ dokku letsencrypt api-test
```

{{< hint warning >}}
**Remember**  
Before doing this script, please make sure you can access the site via `http`.  
Turn off the `Full` Mode for Temporary on the Cloudflare.
{{< /hint >}}