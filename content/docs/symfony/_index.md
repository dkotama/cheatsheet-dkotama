---
bookCollapseSection: true
# draft: true
weight: 20
---

# Symfony Cheatsheet

## Instalation
---
### Method 1 - Using Composer (Symfony 4 and below)
Make sure PHP enviroment is exist , and [Composer](https://getcomposer.org/) is installed.
for this one i use website-skeleton symfony, for basic MVC setup
```bash
composer create-project symfony/website-skeleton project-name
```
to run this project , we should install `web-server-bundle` package. This package only exist on symfony version 4 and below, on symfony 5 the preferred way is using symfony CLI (Method 2).

```bash
composer require --dev symfony/web-server-bundle
```
and run the project using `php bin/console server:run`


### Method 2 - Using symfony CLI

Please refer [here](https://symfony.com/download) to installation symfony CLI

### Create new 
```bash
symfony new project-name
```
and to start the project run `symfony serve` project will run at http://localhost:8000

