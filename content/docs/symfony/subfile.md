---
title: Subfile Template
# draft: true
weight: 1
---

# Subfile Title

## Subfile Title Heading 2
Explanation lorem ipsum dolor sit oiweuo kajs liuoweilsla asldkxzm

### Heading can be link [too](https://github.com/dokku/dokku-mariadb/releases)
Explanation lorem ipsum dolor sit oiweuo kajs liuoweilsla asldkxzm

```javascript
//some javascript 
//just put type besides ``` which language you consider to render
// refer to this docs
// https://gohugo.io/content-management/syntax-highlighting/#list-of-chroma-highlighting-languages


function test(a, b) {
	return a + b;
}
```

{{< hint warning >}}
**Warning Hint**  
As issues [#86: NO CONNECTION](https://github.com/dokku/dokku-mariadb/issues/86) still occurs, and we cannot specify which mariadb version we needed. Here's temporary workaround, revert to version 1.5.0