---
title: Known Solves
weight: 1
---

# Known Solves

## Connecting MySQL / Mariadb from Host to Docker Container

To provide a connection between container we should determine docker network using
command 
```bash
docker network create network1
```
and use `network1` name on the yaml file 
under the `networks` setting. After that declare the network on the container properties.

Because docker mac was a virtualized machine, we need to expose the ports that used by the docker machine database 
in this case `3306`, we exposed it to the port `33061` in case host already have a running service on `3306`.
Rebuild the container, and test the running service on port by command 
```bash
lsof -i :8080

# If success should return
# com.docke  8508 dkotama   63u  IPv6 0xd91677de8e989679      0t0  TCP localhost:33061->localhost:56049 (ESTABLISHED)
```

sample docker-compose.yaml setting
```yaml
  db:
    image: mysql
    restart: always
    environment:
      MYSQL_ROOT_PASSWORD: root
    networks:
      - docknet
    ports: 
      - 33061:3306
    volumes:
      - mariadb1:/var/lib/mysql
      - ./config:/etc/mysql/conf.d

  adminer:
    image: adminer
    restart: always
    ports:
      - 5000:8080
    networks:
      - docknet

networks:
    docknet:
        external: true


volumes:
  mariadb1:
    external: true
```
