---
title: Common Plugins
weight: 1
---

# Common Plugins

## Actuator

### Installing
on `pom.xml`
```xml
<dependencies>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-actuator</artifactId>
    </dependency>
</dependencies>
```

on `build.gradle`
```js
dependencies {
    implementation 'org.springframework.boot:spring-boot-starter-actuator'
}
```

### Configuring
on `application.yml`

```yml
management:
  server:
    port: 9001
    address: 127.0.0.1
  endpoints:
    enabled-by-default: true
    web:
      exposure:
        include: '*'
```



