---
bookCollapseSection: true
weight: 20
---

# Spring Basics


# Installing Spring Initalizr on Mac

Installing via homebrew

``` bash
brew tap pivotal/tap
brew install springboot
```
If you have both Ruby on Rails and Java Spring boot on the mac,
when you type spring command, it will run `spring` package command from Ruby.
To avoid that, let's do symlink from Java spring to springboot, and rename the command

```bash
cd /usr/local/bin
sudo ln -sf ../Cellar/springboot/*.RELEASE/bin/spring ./springboot
```

Now instead running `spring` command, run `springboot` :
```bash
springboot init --name=my-project --dependencies=web,data-jpa,mysql,devtools,thymeleaf --package-name=com.myproject TestApp
```

Thanks to this github [issue solution](https://github.com/spring-guides/gs-spring-boot/issues/17#issuecomment-308879160)