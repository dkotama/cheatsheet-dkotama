---
bookCollapseSection: true
# draft: true
weight: 20
---

# Title 

## Heading 2 
Explanation lorem ipsum dolor sit 

### Heading 3
[Link to ](https://github.com/dokku/dokku/releases)
```bash
Some bash command
```

{{< hint info >}}
**Hint Info**  
If you run `$ dokku storage:report instance` it will show the folder on `deploy` and `run` section.
This means the mounting will be performed only on deploy and run. Any folder or file access that need to 
change the mounted volume should be done on this part
{{< /hint >}}

